#!/bin/sh

ssh -o StrictHostKeyChecking=no root@$IP_ADDRESS << 'ENDSSH'
  cd /srv/www/wordpress-base
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull $IMAGE:web
  docker-compose -f docker-compose.prod.yml up -d
ENDSSH
